package com.example.tot_5.calcu;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

public class calcu extends AppCompatActivity {
    public int resul;
    CheckBox ch1,ch2,ch3,ch4,ch5,ch6,ch7,ch8;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calcu);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        ch1=(CheckBox)findViewById(R.id.ch1);
        ch2=(CheckBox)findViewById(R.id.ch2);
        ch3=(CheckBox)findViewById(R.id.ch3);
        ch4=(CheckBox)findViewById(R.id.ch4);
        ch5=(CheckBox)findViewById(R.id.ch5);
        ch6=(CheckBox)findViewById(R.id.ch6);
        ch7=(CheckBox)findViewById(R.id.ch7);
        ch8=(CheckBox)findViewById(R.id.ch8);




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_calcu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void calcular(View view) {
        int resul;
        int c1,c2,c3,c4,c5,c6,c7,c8;

        if (ch1.isChecked()) {
            c1=1;

        }
        else{
            c1=0;
        }

        if (ch2.isChecked()) {
            c2=2;

        }
        else{
            c2=0;
        }

        if (ch3.isChecked()) {
            c3=4;

        }
        else{
            c3=0;
        }

        if (ch4.isChecked()) {
            c4=8;

        }
        else{
            c4=0;
        }

        if (ch5.isChecked()) {
            c5=16;

        }
        else{
            c5=0;
        }


        if (ch6.isChecked()) {
            c6=32;

        }
        else{
            c6=0;
        }

        if (ch7.isChecked()) {
            c7=64;

        }
        else{
            c7=0;
        }

        if (ch8.isChecked()) {
            c8=128;

        }
        else{
            c8=0;
        }


        resul=c1+c2+c3+c4+c5+c6+c7+c8;

        Toast m = Toast.makeText(getApplicationContext(), String.valueOf(resul),Toast.LENGTH_SHORT);
        m.show();



    }
}
